use rust_decimal::prelude::*;
use std::rc::Rc;
use asrack_core::{AssetAmount, AssetId, Transaction, TransactionDetail, UsdAmount};
use yew::html::IntoPropValue;
use yew::prelude::*;
use crate::matlize::date_picker;
use crate::matlize::prelude::*;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum TradeType {
	Buy,
	Sell,
}

impl IntoPropValue<TradeType> for String {
	fn into_prop_value(self) -> TradeType {
		match self.as_str() {
			"buy" => TradeType::Buy,
			"sell" => TradeType::Sell,
			_ => panic!("Invalid value")
		}
	}
}

#[derive(Clone, PartialEq, Properties)]
pub struct EditTradeProps {
	pub trade_type: TradeType,
	pub container_id: String,
	pub on_save: Callback<Transaction>,
}

#[derive(Debug)]
enum EditTradeAction {
	Submit,
	Date(String),
	Place(String),
	Asset(String),
	Shares(String),
	DollarValue(String),
}

#[derive(Clone, PartialEq, Debug)]
struct EditTradeState {
	pub trade_type: TradeType,
	pub on_save: Callback<Transaction>,
	pub era: u64,
	pub date: String,
	pub place: String,
	pub asset: String,
	pub shares: String,
	pub dollar_value: String,
}

impl EditTradeState {
	fn new_generation(&self) -> Self {
		Self {
			trade_type: self.trade_type,
			on_save: self.on_save.clone(),
			era: self.era + 1,
			date: date_picker::today(),
			place: "".to_string(),
			asset: "".to_string(),
			shares: "".to_string(),
			dollar_value: "".to_string(),
		}
	}
}

impl Reducible for EditTradeState {
	type Action = EditTradeAction;

	fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
		web_sys::console::log_1(&format!("EDIT ACTION: {:?}\nSTATE: {:?}", &action, &self).into());
		let next = match action {
			EditTradeAction::Submit => {
				let transaction = {
					let date = date_picker::parse_date(&self.date).ok();
					let place = (!self.place.is_empty()).then_some(self.place.clone());
					let detail = {
						let asset_amount = {
							let symbol = (!self.asset.is_empty()).then_some(AssetId::from(&self.asset));
							let amount = Decimal::from_str(&self.shares).ok();
							symbol.zip(amount).map(|(asset, amount)| {
								AssetAmount(asset, amount)
							})
						};
						let usd_value = Decimal::from_str(&self.dollar_value).map(|x| UsdAmount::from(&x)).ok();
						asset_amount.zip(usd_value).map(|(asset_amount, usd_value)| {
							match self.trade_type {
								TradeType::Buy => TransactionDetail::Buy { asset_amount, usd_cost: usd_value },
								TradeType::Sell => TransactionDetail::Sell { asset_amount, usd_proceeds: usd_value },
							}
						})
					};
					if let (Some(time), Some(place), Some(detail)) = (date, place, detail) {
						Some(Transaction { time, place, detail })
					} else { None }
				};
				if let Some(transaction) = transaction {
					self.on_save.emit(transaction);
					self.new_generation()
				} else {
					self.as_ref().clone()
				}
			}
			EditTradeAction::Date(s) => Self { date: s, ..self.as_ref().clone() },
			EditTradeAction::Place(s) => Self { place: s, ..self.as_ref().clone() },
			EditTradeAction::Asset(s) => Self { asset: s.to_uppercase(), ..self.as_ref().clone() },
			EditTradeAction::Shares(s) => Self { shares: s, ..self.as_ref().clone() },
			EditTradeAction::DollarValue(s) => Self { dollar_value: s, ..self.as_ref().clone() },
		};
		web_sys::console::log_1(&format!("NEXT: {:?}", next).into());
		next.into()
	}
}

#[function_component(EditTrade)]
pub fn component(props: &EditTradeProps) -> Html {
	let state = use_reducer_eq(|| {
		EditTradeState {
			trade_type: props.trade_type,
			on_save: props.on_save.clone(),
			era: 0,
			date: date_picker::today(),
			place: "".to_string(),
			asset: "".to_string(),
			shares: "".to_string(),
			dollar_value: "".to_string(),
		}
	});
	let onsubmit = {
		let state = state.clone();
		Callback::from(move |_| state.dispatch(EditTradeAction::Submit))
	};
	let onchange_date = {
		let state = state.clone();
		Callback::from(move |s: String| state.dispatch(EditTradeAction::Date(s)))
	};
	let onchange_place = {
		let state = state.clone();
		Callback::from(move |s: String| state.dispatch(EditTradeAction::Place(s)))
	};
	let onchange_asset = {
		let state = state.clone();
		Callback::from(move |s: String| state.dispatch(EditTradeAction::Asset(s)))
	};
	let onchange_shares = {
		let state = state.clone();
		Callback::from(move |s: String| state.dispatch(EditTradeAction::Shares(s)))
	};
	let onchange_dollar_value = {
		let state = state.clone();
		Callback::from(move |s: String| state.dispatch(EditTradeAction::DollarValue(s)))
	};
	let (title, action, modal_id) = match props.trade_type {
		TradeType::Buy => ("New Buy".to_string(), "Add Buy".to_string(), "add-buy".to_string()),
		TradeType::Sell => ("New Sell".to_string(), "Add Sell".to_string(), "add-sell".to_string()),
	};
	let modal_id = format!("{}-{}", modal_id, state.era);
	html! {
		<>
		<ModalDialog id={modal_id.clone()}>
			<Form {onsubmit} age={state.era}>
				<div class="modal-content">
					<h4>{title}
						<a href="#!" class="modal-close waves-effect waves-green btn-flat right">
							<i class="material-icons">{"close"}</i>
						</a>
					</h4>
					<div class="section">
						<DatePicker id="trade_date" label="Date" value={state.date.to_string()} onchange={onchange_date} container_id={props.container_id.clone()} />
						<Input id="trade_place" label="Where" value={state.place.to_string()} onchange={onchange_place}/>
						<Input id="trade_asset" label="Asset" value={state.asset.to_string()} onchange={onchange_asset}/>
						<Input id="trade_shares" label="Shares" value={state.shares.to_string()} onchange={onchange_shares} in_type="number"/>
						<Input id="trade_value" label="Dollar Value" value={state.dollar_value.to_string()} onchange={onchange_dollar_value} in_type="number"/>
					</div>
					<input class="waves-effect waves-green btn" type="submit" value={action.clone()}/>
				</div>
			</Form>
		</ModalDialog>
		<ModalTrigger id={modal_id.clone()} label={format!("+ {}", action.clone())}/>
		</>
	}
}

