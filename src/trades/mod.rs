use std::rc::Rc;
use asrack_core::{Transaction, TransactionDetail};


use yew::prelude::*;
use crate::core::{format_asset_amount, format_date_only, format_usd_amount};
use crate::top_nav::*;
use crate::matlize::prelude::*;
use crate::trades::edit::TradeType;
use self::edit::EditTrade;

#[derive(Debug)]
enum AllTradesAction {
	AddTransaction(Transaction),
}

#[derive(Clone, PartialEq, Debug)]
struct AllTradesState {
	pub transactions: Vec<Transaction>,
}

impl Reducible for AllTradesState {
	type Action = AllTradesAction;

	fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
		web_sys::console::log_1(&format!("ACTION, STATE: {:?}, {:?}", &action, &self).into());
		let next = match action {
			AllTradesAction::AddTransaction(transaction) => {
				web_sys::console::log_1(&format!("SAVE: {:?}", &transaction).into());
				let mut transactions = self.transactions.clone();
				transactions.push(transaction);
				Self { transactions, ..self.as_ref().clone() }
			}
		};
		web_sys::console::log_1(&format!("NEXT: {:?}", &next).into());
		next.into()
	}
}

#[function_component(AllTrades)]
pub fn component() -> Html {
	let state = use_reducer_eq(|| {
		let transactions = Vec::new();
		AllTradesState { transactions }
	});
	let on_save_buy: Callback<Transaction> = {
		let state = state.clone();
		Callback::from(move |x| state.dispatch(AllTradesAction::AddTransaction(x)))
	};
	let on_save_sell = on_save_buy.clone();
	html! {
		<>
		<TopNav active_page={TopPage::Trades}/>
		<div id="trades-content" class="section">
			<div class="container">
					<h4>{"Active Trades"}</h4>
					<div class="section">
						{html_trades_table(&state.transactions)}
						<p>{format!("{} trades", state.transactions.len())}</p>
					</div>
					<div class="section">
			            <EditTrade trade_type={TradeType::Buy} container_id="trades-content" on_save={on_save_buy}/>
						{EM_SPACE}
						<EditTrade trade_type={TradeType::Sell} container_id="trades-content" on_save={on_save_sell}/>
					</div>
			</div>
		</div>
		</>
	}
}

fn html_trades_table(txns: &Vec<Transaction>) -> Html {
	let table_rows = txns.iter().map(html_trade_row).collect::<Vec<_>>();
	html! {
	<table>
	<thead>
		<tr><th>{"When"}</th><th>{"In"}</th><th>{"Out"}</th><th>{"At"}</th></tr>
	</thead>
	<tbody class="collection">
		{table_rows}
	</tbody>
	</table>
	}
}

fn html_trade_row(txn: &Transaction) -> Html {
	let when = format_date_only(&txn.time);
	let where_at = txn.place.to_string();
	let (what_from, what_to) = match &txn.detail {
		TransactionDetail::Buy { asset_amount, usd_cost } => {
			let what_from = format_usd_amount(usd_cost);
			let what_to = format_asset_amount(asset_amount);
			(what_from, what_to)
		}
		TransactionDetail::Sell { asset_amount, usd_proceeds } => {
			let what_to = format_usd_amount(usd_proceeds);
			let what_from = format_asset_amount(asset_amount);
			(what_from, what_to)
		}
	};
	html! {
		<tr class="collection - item">
		<td>{when}</td><td>{what_to}</td><td>{what_from}</td><td>{where_at}</td>
		</tr>
	}
}

pub mod edit;