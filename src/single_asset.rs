use yew::prelude::*;
use yew_router::prelude::*;
use crate::top_nav::*;
use crate::Route;

#[derive(Properties, PartialEq)]
pub struct Props {
	pub asset_id: String,
}

#[function_component(SingleAsset)]
pub fn component(props: &Props) -> Html {
	let asset_id = props.asset_id.to_owned();
	let card = html! {
			<div class="card">
				<div class="card-content grey lighten-5">
					<h6>{ "7 shares held"}</h6>
					<h6>{ "$22 basis"}</h6>
				</div>
			</div>
		};
	html! {
	<>
	<TopNav active_page={TopPage::Assets}/>
	<div class="container">
		<div class="section">
			<Link<Route> to={Route::AllAssets} classes="waves-effect waves-light btn-flat">
				<i class="large material-icons left">{"arrow_back"}</i>{"Back"}
				</Link<Route>>
			<h3>{asset_id}</h3>
			{ card }
			</div>
		<div class="section">
			<h5>{"Held Lots"}</h5>
			<table>
			<thead>
			<tr>
			<th>{"Date"}</th>
			<th>{"Location"}</th>
			<th>{"Bought"}</th>
			<th>{"Unsold"}</th>
			<th>{"Mkt Value"}</th>
			<th>{"Basis"}</th>
			</tr>
			</thead>
			<tbody>
				<tr>
					<td>{"2007-01-01"}</td>
					<td>{"Mars"}</td>
					<td>{"35"}</td>
					<td>{"7"}</td>
					<td>{"$20"}</td>
					<td>{"$10"}</td>
				</tr>
			</tbody>
			</table>
		</div>
		<div class="section">
			<h5>{"Sold Lots"}</h5>
			{"—"}
		</div>
	</div>
	</>
	}
}
