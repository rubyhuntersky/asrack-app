use yew::prelude::*;
use yew_router::prelude::*;
use crate::Route;

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum TopPage {
	Assets,
	Trades,
	Pools,
}

#[derive(Properties, PartialEq)]
pub struct Props {
	pub active_page: TopPage,
}

#[function_component(TopNav)]
pub fn component(props: &Props) -> Html {
	let items = list_items(props.active_page)
		.into_iter()
		.map(|item| {
			html! {
				if item.is_active {
					<li class="active"><Link<Route> to={item.route}>{item.label}</Link<Route>></li>
				} else {
					<li><Link<Route> to={item.route}>{item.label}</Link<Route>></li>
				}
			}
		})
		.collect::<Html>();
	html! {
<header>
<nav>
	<div class="nav-wrapper">
		<Link<Route> to={Route::AllAssets} classes="brand-logo">{"  ⦀ asrack"}</Link<Route>>
		<ul id="nav-mobile" class="right">
			{ items }
		</ul>
	</div>
</nav>
</header>
	}
}

fn list_items(active_page: TopPage) -> Vec<ListItem> {
	let items = vec![
		ListItem {
			_page: TopPage::Assets,
			is_active: active_page == TopPage::Assets,
			route: Route::AllAssets,
			label: "Assets".to_string(),
		},
		ListItem {
			_page: TopPage::Trades,
			is_active: active_page == TopPage::Trades,
			route: Route::AllTrades,
			label: "Trades".to_string(),
		},
		ListItem {
			_page: TopPage::Pools,
			is_active: active_page == TopPage::Pools,
			route: Route::NotFound,
			label: "Pools".to_string(),
		},
	];
	items
}


struct ListItem {
	_page: TopPage,
	is_active: bool,
	route: Route,
	label: String,
}
