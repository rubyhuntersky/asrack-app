use wasm_bindgen::prelude::*;
use web_sys::{EventInit, HtmlFormElement};
use yew::prelude::*;
use yew::{html, Html, Properties};

#[wasm_bindgen]
extern "C" {
	#[wasm_bindgen(js_name = updateTextFields, js_namespace = M)]
	fn js_update_text_fields();
}

#[derive(Clone, PartialEq, Properties)]
pub struct FormProps {
	pub onsubmit: Callback<()>,
	pub children: Children,
	#[prop_or(0)]
	pub age: u64,
}

#[function_component(Form)]
pub fn component(props: &FormProps) -> Html {
	let form_ref = use_node_ref();
	let onsubmit = {
		let form_ref = form_ref.clone();
		let props_submit = props.onsubmit.clone();
		Callback::from(move |_| {
			let mut event_init = EventInit::new();
			event_init.bubbles(true);
			let event = Event::new_with_event_init_dict("form_done", &event_init).expect("new event");
			let form = form_ref.cast::<HtmlFormElement>().expect("form is html element");
			form.dispatch_event(&event).expect("event dispatch");
			props_submit.emit(());
		})
	};
	use_effect_with_deps(|_| js_update_text_fields(), props.age);
	html! {
		<form action={PREVENT_PAGE_REFRESH_ACTION} {onsubmit} ref={form_ref}>
	        { for props.children.iter() }
		</form>
	}
}

const PREVENT_PAGE_REFRESH_ACTION: &'static str = "#!";
