use std::rc::Rc;
use wasm_bindgen::JsCast;
use wasm_bindgen::prelude::Closure;
use web_sys::HtmlElement;
use yew::prelude::*;
use yew::{html, Html, Properties, Children};
use crate::matlize::{close_modal, init_modal1};

#[derive(Properties, PartialEq)]
pub struct ModalDialogProps {
	pub id: String,
	pub children: Children,
}

pub enum ModalDialogAction {
	Exit(u64, NodeRef)
}

#[derive(Clone, PartialEq, Debug)]
struct ModalDialogState {
	era: u64,
}

impl Default for ModalDialogState {
	fn default() -> ModalDialogState {
		Self { era: 0 }
	}
}

impl Reducible for ModalDialogState {
	type Action = ModalDialogAction;

	fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
		match action {
			ModalDialogAction::Exit(era, node_ref) => {
				if era == self.era {
					web_sys::console::log_1(&"Modal exit!".into());
					let elem = node_ref.cast::<HtmlElement>().expect("html element from dialog node");
					close_modal(&elem);
				}
			}
		}
		self
	}
}

impl ModalDialogProps {
	pub fn modal_id(&self) -> String { format!("modal-{}", &self.id) }
}

#[function_component(ModalDialog)]
pub fn component(props: &ModalDialogProps) -> Html {
	let modal_id = props.modal_id();
	let node_ref = use_node_ref();
	{
		use_reducer_eq(ModalDialogState::default);
		use_effect_with_deps(
			move |dialog_node| {
				let form_done_listener;
				let elem = dialog_node.cast::<HtmlElement>().expect("modal is html element");
				{
					let modal_ref = dialog_node.clone();
					let listener = Closure::<dyn Fn(Event)>::wrap(Box::new(move |_e: Event| {
						let modal = modal_ref.cast::<HtmlElement>().expect("modal is html element");
						close_modal(&modal);
						web_sys::console::log_1(&"form_done event".into());
					}));
					elem.add_event_listener_with_callback("form_done", listener.as_ref().unchecked_ref()).expect("add form_done listener");
					form_done_listener = Some(listener);
				}
				init_modal1(&elem);
				move || drop(form_done_listener)
			}, node_ref.clone());
	}
	html! {
		<div class="modal" id={modal_id} ref={node_ref} >
            { for props.children.iter() }
		</div>
    }
}

#[derive(Properties, PartialEq)]
pub struct ModalTriggerProps {
	pub id: String,
	pub label: String,
}

impl ModalTriggerProps {
	pub fn modal_id(&self) -> String { format!("modal-{}", &self.id) }
}

pub struct ModalTrigger {}

impl Component for ModalTrigger {
	type Message = ();
	type Properties = ModalTriggerProps;

	fn create(_ctx: &Context<Self>) -> Self {
		ModalTrigger {}
	}

	fn view(&self, ctx: &Context<Self>) -> Html {
		let props = ctx.props();
		html! {
		<button data-target={props.modal_id()} class="btn modal-trigger">{&props.label}</button>
		}
	}
}
