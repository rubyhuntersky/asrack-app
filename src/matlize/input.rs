use yew::prelude::*;
use yew::{html, Html, Properties};
use crate::matlize::onchange_emit_string;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
	pub id: String,
	#[prop_or("text".to_string())]
	pub in_type: String,
	pub label: String,
	pub value: String,
	pub onchange: Callback<String>,
}

#[function_component(Input)]
pub fn component(props: &Props) -> Html {
	let input_ref = use_node_ref();
	let onchange = onchange_emit_string(&props.onchange);
	let value = props.value.to_string();
	let in_type = props.in_type.to_string();
	html! {
		<div class="input-field">
			<input id={props.id.to_string()}
				type={in_type}
				class="validate"
				required=true
				{value}
				{onchange}
				ref={input_ref} />
			<label for={props.id.to_string()}>{props.label.to_string()}</label>
		</div>
	}
}
