use wasm_bindgen::prelude::*;
use web_sys::{HtmlElement, HtmlInputElement};
use yew::Callback;
use yew::html::onchange;

#[wasm_bindgen]
extern "C" {
	fn init_modal1(elem: &HtmlElement);
	fn close_modal(elem: &HtmlElement);
}

pub mod modal;
pub mod date_picker;
pub mod input;
pub mod form;

pub const EM_SPACE: &'static str = "\u{2003}";

pub mod prelude {
	use wasm_bindgen::prelude::*;
	use web_sys::Element;
	pub use super::date_picker::DatePicker;
	pub use super::input::Input;
	pub use super::modal::{ModalDialog, ModalTrigger};
	pub use super::form::Form;
	pub use super::EM_SPACE;

	pub fn dom_element_with_id(container_id: &str) -> Element {
		let window = web_sys::window().expect("global window does not exists");
		let document = window.document().expect("expecting a document on window");
		let gotten = document.get_element_by_id(container_id).expect("element with id");
		let container = gotten.dyn_into::<Element>().expect("cast to Element");
		container
	}
}

pub fn onchange_emit_string(callback: &Callback<String>) -> Callback<onchange::Event> {
	let onchange = callback.clone();
	Callback::from(move |e: onchange::Event| {
		let s = e.target().unwrap().unchecked_into::<HtmlInputElement>().value();
		let s = s.trim().to_string();
		onchange.emit(s);
	})
}
