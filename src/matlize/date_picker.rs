use chrono::{DateTime, Local, NaiveDate, NaiveTime, ParseError, TimeZone};
use js_sys::{Object, Reflect};
use wasm_bindgen::prelude::*;
use web_sys::{Element, HtmlElement};
use yew::prelude::*;
use yew::{html, Html, Properties};
use crate::matlize::{onchange_emit_string};
use crate::matlize::prelude::dom_element_with_id;

#[wasm_bindgen]
extern "C" {
	#[wasm_bindgen(js_name = init, js_namespace = ["M", "Datepicker"])]
	pub fn js_init_datepicker(elem: &HtmlElement, settings: Object);
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
	pub id: String,
	pub label: String,
	pub value: String,
	pub onchange: Callback<String>,
	pub container_id: String,
}


#[function_component(DatePicker)]
pub fn component(props: &Props) -> Html {
	let input_ref = use_node_ref();
	{
		let container_id = props.container_id.clone();
		let input_ref = input_ref.clone();
		use_effect_with_deps(move |input_ref| {
			let input = input_ref.cast::<HtmlElement>().expect("input_ref not attached to input element");
			let container = dom_element_with_id(&container_id);
			let auto_close = true;
			let settings = datepicker_settings(&container, auto_close);
			js_init_datepicker(&input, settings.into());
		}, input_ref);
	}
	let input_id = props.id.clone();
	let value = props.value.clone();
	let onchange = onchange_emit_string(&props.onchange);
	html! {
		<div class="input-field">
			<input id={input_id.clone()}
				type="text"
				class="datepicker validate"
				required=true
				{value}
				{onchange}
				ref={input_ref}/>
			<label for={input_id.clone()}>{props.label.clone()}</label>
		</div>
	}
}

const FORMAT: &'static str = "%b %e, %Y";

pub fn format_date(date: &DateTime<Local>) -> String {
	date.format(FORMAT).to_string()
}

pub fn parse_date(s: impl AsRef<str>) -> Result<DateTime<Local>, ParseError> {
	NaiveDate::parse_from_str(s.as_ref(), FORMAT).map(|date| {
		let naive = date.and_time(NaiveTime::default());
		let local: DateTime<Local> = Local.from_local_datetime(&naive).unwrap();
		local
	})
}

pub fn today() -> String { format_date(&Local::now()) }

fn datepicker_settings(container: &Element, auto_close: bool) -> Object {
	let settings = Object::new();
	Reflect::set(&settings, &JsValue::from_str("autoClose"), &JsValue::from_bool(auto_close)).unwrap();
	Reflect::set(&settings, &JsValue::from_str("container"), container.as_ref()).unwrap();
	settings
}
