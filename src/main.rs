use yew::prelude::*;
use yew_router::prelude::*;
use single_asset::*;
use all_assets::*;
use trades::*;

mod core;

mod single_asset;
mod all_assets;
mod top_nav;
mod trades;
pub mod matlize;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
	#[at("/")]
	AllAssets,
	#[at("/assets/:id")]
	SingleAsset { id: String },
	#[at("/trades")]
	AllTrades,
	#[not_found]
	#[at("/404")]
	NotFound,
}

fn switch(routes: Route) -> Html {
	match routes {
		Route::NotFound => html! { <h1>{ "404" }</h1> },
		Route::AllAssets => html! { <AllAssets /> },
		Route::SingleAsset { id } => html! {<SingleAsset asset_id={id}/>},
		Route::AllTrades => html! {<AllTrades />},
	}
}

#[function_component(App)]
fn app() -> Html {
	html! {
        <BrowserRouter>
            <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
        </BrowserRouter>
    }
}

fn main() {
	yew::Renderer::<App>::new().render();
}
