use asrack_core::{AssetAmount, AssetId, UsdAmount};
use chrono::{DateTime, Local};

pub fn format_asset_amount(asset_amount: &AssetAmount) -> String {
	format!("{} {}", asset_amount.1.to_string(), format_asset_id(&asset_amount.0))
}

pub fn format_usd_amount(amount: &UsdAmount) -> String {
	format!("{} USD", amount.to_decimal().to_string())
}

pub fn format_asset_id(asset_id: &AssetId) -> String {
	match asset_id {
		AssetId::Usd => "USD".to_string(),
		AssetId::Symbol(s) => s.to_string(),
	}
}

pub fn format_date_only(date_time: &DateTime<Local>) -> String {
	date_time.format("%Y-%m-%d").to_string()
}
