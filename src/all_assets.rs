use yew::prelude::*;
use yew_router::prelude::*;
use crate::top_nav::*;
use crate::Route;

#[function_component(AllAssets)]
pub fn component() -> Html {
	html! {
	<>
	<TopNav active_page={TopPage::Assets}/>
	<div class="container">
	<div class="section">
		<h3>{ "In Hand" }</h3>
		<div class="collection">
			<Link<Route>
			 to={Route::SingleAsset{id:"SHIB".into()}}
			 classes="collection-item"
			>
			{"SHIB"}
			</Link<Route>>
			<Link<Route> to={Route::SingleAsset{id:"DOGE".into()}} classes="collection-item">{"DOGE"}</Link<Route>>
			<a href="#!" class="collection-item">{"Alvin"}</a>
			<a href="#!" class="collection-item">{"Alvin"}</a>
			<a href="#!" class="collection-item">{"Alvin"}</a>
		</div>
	</div>
	<div class="section">
		<h5>{ "Previously Held" }</h5>
		{"—"}
	</div>
		<button data-target="modal1" class="btn-floating btn-large modal-trigger waves-effect waves-light right"><i class="material-icons">{"add"}</i></button>
		<div id="modal1" class="modal">
			<div class="modal-content">
				<h4>{"Modal Header"}</h4>
				<p>{"A bunch of text"}</p>
			</div>
			<div class="modal-footer">
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">{"Agree"}</a>
			</div>
		</div>
	</div>
	<script>
{"
	    document.addEventListener('DOMContentLoaded', function() {
	        // M.AutoInit();
	        var elems = document.querySelectorAll('.modal');
	        var instances = M.Modal.init(elems);
	    });
"}
	</script>
	</>
}
}
